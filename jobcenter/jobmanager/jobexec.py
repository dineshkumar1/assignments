import importlib
import importlib.util
import sys
import os
import json

class dynaobject(dict):
    __getattr__ = dict.__getitem__
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__



class functionlibrary():
    def __init__(self):
        pass
    def loadmodule(self, modulename):
        mod = importlib.import_module("."+modulename,"functions.python")
        return mod


funclib = functionlibrary()
import sys
sys.path.append('../')


def module_from_file(module_name, file_path):
    spec = importlib.util.spec_from_file_location(module_name, os.path.join(file_path, module_name, '__init__.py'))
    module = importlib.util.module_from_spec(spec)
    sys.modules[module_name] = module
    spec.loader.exec_module(module)
    return module

try:
    dbconfig = json.loads(str(sys.argv[3]))
    eventdata = json.loads(str(sys.argv[4]))
    dbconnections = importlib.import_module('.dbfunctions', 'functions.python')
    dbconnections.config(dbconfig)
    # importlib.import_module(".."+sys.argv[1],'jobfunctions.python.functions').handler(sys.argv[1], funclib, dbconnections)
    module_from_file(sys.argv[1],sys.argv[2]).handler(sys.argv[1], funclib, dbconnections)
except  Exception as e:
    print(e)
