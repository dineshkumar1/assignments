const fs = require("fs");
const csvjson = require('csvjson');


module.exports = library => {
  
  library.add("savedatatocsv", (dataset, filename) => {
    try {
      var data=[];
        for (let index = 0; index < dataset.length; ++index) {
               data.push(dataset[index])
           } 
        const csvData = csvjson.toCSV(data, {
            headers: 'key'
        });
        fs.writeFileSync(filename, csvData);   
    } catch (error) {
        logger.log('Error occurred - '+ error)
        return false
    }
    return true
   });
};
