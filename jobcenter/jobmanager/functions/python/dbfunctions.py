from sqlalchemy import create_engine
from sqlalchemy import Table, Column, Integer, String, MetaData, ForeignKey
from sqlalchemy import inspect
from sqlalchemy.sql import text

dbconnections = {}
dbconfig = None

def config(config):
    global dbconfig
    dbconfig = config

class dbconnection():
    global dbconfig
    dbengine = None
    def __init__(self, dbname):
        if dbname is None:
            dbname = dbconfig.databases.defaultdb
        
        connstr = 'oracle://'+dbconfig[dbname]['user']+':'+dbconfig[dbname]['password']+'@'+dbconfig[dbname]['connectstring']
        self.dbengine = create_engine(
            connstr,
            convert_unicode=False,
            # pool_recycle=10,
            # pool_size=50,
            echo=False
        )
    def getdata(self,stmt):
        try:
            with self.dbengine.connect() as con:
                statement = text(stmt)
                result = con.execute(statement)
                result.fetchall()
                return result
        except Exception as e:
            print(e)
            return None

def connection(dbname):
    if dbname not in dbconnections:
        dbconnections[dbname] = dbconnection(dbname)
    return dbconnections[dbname]
