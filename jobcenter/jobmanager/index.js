const path = require("path");
const fs = require("fs");
var logger;

global.funclib = {};

var dbconfig;
const jobFunctions = {};
var jobFunctionslib;
const jobDetails = {};
var jobCenterEvents;
var jobs;
var downloadfolder, uploadfolder;


const loadfunctiondir = dirpath => {
  fs.readdirSync(dirpath).forEach(function(file) {
    if (fs.lstatSync(path.join(dirpath, file)).isDirectory()) {
      require(path.join(dirpath, file))({
        add(name, funccode) {
          funclib[name] = funccode;
        }
      });
    }
  });
};

const loadjobFunctions = foldername => {
  let funcmodules = fs.readdirSync(foldername);
  for (let i = 0; i < funcmodules.length; i++) {
    if (fs.lstatSync(path.join(foldername, funcmodules[i])).isDirectory()) {
      // if (path.extname(funcmodules[i]) == ".js") {
      let filepath = path.join(foldername, funcmodules[i]);
      let jobfunc = require(filepath);
      jobDetails[jobfunc.name] = jobfunc;
      jobFunctions[jobfunc.name] = jobfunc.handler;
    }
  }
};

const loadjobs = filename => {
  try {
    jobs = JSON.parse(fs.readFileSync(filename));

    const execnodejob = jobfunc => {
      return async eventdata => {
        try {
          var result = await jobfunc(eventdata);
        } catch (error) {
          logger.error(`Error occrurred ${error}`);
          result = "Error occrurred, check logs";
        }
        return result;
      };
    };

    const execpythonjob = scriptname => {
      var pythonshell = require("python-shell").PythonShell;

      return eventdata => {
        let options = {
          mode: "text",
          pythonOptions: ["-u"], // get print results in real-time
          scriptPath: __dirname,
          args: [
            scriptname,
            path.join(jobFunctionslib, "python", "functions"),
            JSON.stringify(dbconfig),
            JSON.stringify(eventdata)
          ]
        };
        pythonshell.run("jobexec.py", options, function(err, results) {
          if (err) {
            console.log(`Error occurred ${err}`);
          } else {
            console.log("results: %j", results);
          }
        });
      };
    };
    jobs.jobs.forEach(job => {
      if (
        job.trigger == "EVENT" &&
        job.eventname &&
        job.function &&
        jobFunctions[job.function]
      ) {
        let execfunc = job.function.startsWith("python.")
          ? execpythonjob
          : execnodejob;
        jobCenterEvents.on(
          "EVENT." + job.eventname,
          execfunc(jobFunctions[job.function])
        );
      }
      if (job.function) {
        let execfunc = job.function.startsWith("python.")
          ? execpythonjob(job.function.split(".")[1])
          : execnodejob(jobFunctions[job.function]);
        jobCenterEvents.on("JOB." + job.name, execfunc);
      }
    });
  } catch (error) {
    console.log("Can not load job file : " + filename);
  }
};

module.exports = () => {
  var EventEmitter2 = require("eventemitter2").EventEmitter2;
  jobCenterEvents = new EventEmitter2({
    wildcard: true,
    maxListeners: 0
  });
  return {
    init(options) {
      if (!options.jobList) {
        console.log("No job list found !");
        return;
      }
      if (options.logger) logger = options.logger;

      loadfunctiondir(path.join(__dirname, "functions", "node"));

      if (!options.jobFunctions) throw "No job functions defined";
      loadjobFunctions(path.join(options.jobFunctions, "node", "functions"));
      jobFunctionslib = path.join(options.jobFunctions, "");

      if (options.jobList) {
        loadjobs(options.jobList);
      }
      downloadfolder = options.downloadfolder || "downloads";
      uploadfolder = options.uploadfolder || "uploads";

      dbconfig = options.dbconfig;
      global.dbconnections = require("./functions/node/dbfunctions")(dbconfig);
    },
    on(eventname, eventfunc) {
      jobCenterEvents.on(eventname, eventfunc);
    },
    emit() {
      var args = Array.from(arguments);
      let eventname = args[0];
      args.shift();
      jobCenterEvents.emit("EVENT." + eventname, args);
    },
    getJoblist() {
      return jobs;
    },
    geteventlist(event) {
      let array = jobs.jobs
      console.log(array)
      let evt = [];
      array.forEach(job =>{ 
        if(job.eventname == event.eventname){evt.push(job)}
      })
      return {event:evt};
    },
    runJob(jobname, jobdata) {
      jobCenterEvents.emit("JOB." + jobname, jobdata);
      return { "job": jobname, "status": "success", "msg": "Executing"+"  "+jobname};
    },
    triggerEvent(eventname, eventdata) {
      jobCenterEvents.emit("EVENT." + eventname, eventdata);
      return { "event": eventname, "status": "success","msg":"Event"+"  "+eventname+"  "+" has been triggered"};
    },
    functionlist() {
      return Object.keys(jobFunctions);
    }
  };
};
