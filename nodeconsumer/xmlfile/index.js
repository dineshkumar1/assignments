const exec = require('child_process').exec; 

let obj = {
    queuename: 'childprocess',
    handler: function (message) {
        exec('master.bat', (error, stdout, stderr) => {
            if (error) {
                console.error('stderr', stderr);
                throw error;
            }
            console.log("File has been created!");
        });
    }
};

module.exports = obj;