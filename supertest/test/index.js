var chai = require('chai');
var request = require('supertest');
var expect = chai.expect;
var server = require('../prac.js');
const oracledb = require('oracledb');




describe('CRUD Operations', function(){
   
         it('should be connected to database',  function(done){
             connection = oracledb.getConnection({
                user: "cashtrea_testing",
                password: "cashtrea_testing",
                connectString: "ideak6db.chwkrfaqj9m1.ap-south-1.rds.amazonaws.com:1521/ideal6db"
              });
             if(connection) done();
         });

         it('should return the data',  function(done){
          request(server)
            .get("/student")
            .set('Accept', 'application/json')
            .expect(200)
            .end(function(err, res){
              expect(err).to.be.null;
              expect(res.body).to.have.length.greaterThan(1);
              done();
            }); 
       });

       it('should return the particular data',  function(done){
        request(server)
          .get("/student")
          .query({rollno:3})
          .set('Accept', 'application/json')
          .expect(200)
          .end(function(err, res){
            expect(err).to.be.null;
            done();
          }); 
     });

     it('should insert the data',  function(done){
      request(server)
        .post("/student")
        .send({rollno:32,name:'maddy',age:3})
        .set('Accept', 'application/json')
        .expect(200)
        .end(function(err, res){
          expect(err).to.be.null;
          done();
        }); 
       
   });

   it("it shoud return status code 400 if we doesn't send some property", function(done){
    request(server)
      .post("/student")
      .send({name:"Rockie"})
      .set('Accept', 'application/json')
      .expect(400)
      .end(function(err, res){
        if (err) console.log(err);
        done();
      });
  });

  it("should be updated", function(done){
    request(server)
      .patch("/student")
      .query({rollno:3})
      .send({age:24})
      .expect(200)
      .end(function(err, res){
        if (err) console.log(err);
        done();
      });
  });

  it("it should return status code 400 if we doesn't send id", function(done){
    request(server)
      .patch("/student")
      .query({})
      .send({age:24})
      .expect(400)
      .end(function(err, res){
        if (err) console.log(err);
        done();
      });
  });

  it("it should return status code 400 if we doesn't send the data to be updated", function(done){
    request(server)
      .patch("/student")
      .query({rollno:3})
      .send({})
      .expect(400)
      .end(function(err, res){
        if (err) console.log(err);
        done();
      });
  });

  it("it should delete the data", function(done){
    request(server)
      .delete("/student")
      .query({rollno:3})
      .expect(200)
      .end(function(err, res){
        if (err) console.log(err);
      });
      done();
  });

  it("it should return status code 400 if we doesn't send the id", function(done){
    request(server)
      .delete("/student")
      .query({})
      .expect(400)
      .end(function(err, res){
        if (err) console.log(err);
      });
      done();
  });
 });
