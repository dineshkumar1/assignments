const express = require('express')
const oracledb = require('oracledb');
var bodyparser = require('body-parser')
oracledb.autoCommit = true;
const app = express();
const port = 3000;

app.use(bodyparser())

app.get('/student',async function (req, res) {
  try {
     connection = await oracledb.getConnection({
      user: "cashtrea_testing",
      password: "cashtrea_testing",
      connectString: "ideak6db.chwkrfaqj9m1.ap-south-1.rds.amazonaws.com:1521/ideal6db"
    });
    if(req.query.rollno == undefined || req.query.rollno == null || req.query.rollno == ''){
      var result = await connection.execute(`SELECT * FROM student`);
      if(result){
        res.send(result.rows)
      }
    }else{
      let id = req.query.rollno;
    if (isNaN(id)) {
      res.send('Query param rollno is not number')
      return
    }
  
      var result = await connection.execute(`SELECT * FROM student where rollno=:id`, [id]);
      if(result){
        res.send(result.rows)
      }
      }
    } catch (err) {
      //send error message
      return res.send(err.message);
    }
})

app.patch('/student',async function (req, res) {
  try {
     connection = await oracledb.getConnection({
      user: "cashtrea_testing",
      password: "cashtrea_testing",
      connectString: "ideak6db.chwkrfaqj9m1.ap-south-1.rds.amazonaws.com:1521/ideal6db"
    });
    var id = req.query.rollno;
    if (isNaN(id)) {
      res.status(400);
      res.send('Query param rollno is not number')
      return
    }else if(req.body.age == undefined){
      res.status(400);
      res.send("Enter the data to be updated!");
    }
    else{
      var age = req.body.age;
      var result = await connection.execute(`UPDATE student SET age=:age WHERE rollno=:id`, [age,id]);
      if(result){
        res.send("Data has been updated!")
      }
    }
     
  }catch (err) {
    //send error message
    return res.send(err.message);
  }
})

app.delete('/student',async function (req, res) {
  try {
     connection = await oracledb.getConnection({
      user: "cashtrea_testing",
      password: "cashtrea_testing",
      connectString: "ideak6db.chwkrfaqj9m1.ap-south-1.rds.amazonaws.com:1521/ideal6db"
    });
    let id = req.query.rollno;
    if (isNaN(id)) {
      res.status(400);
      res.send('Query param rollno is not number')
      return
    }else{
      var result = await connection.execute(`DELETE FROM student WHERE rollno=:id`, [id]);
      if(result){
        res.send("Data has been deleted!")
      }
    } 
  }catch (err) {
    //send error message
    return res.send(err.message);
  }
})


app.post('/student',async function (req, res) {
  try {
    var connection = await oracledb.getConnection({
      user: "cashtrea_testing",
      password: "cashtrea_testing",
      connectString: "ideak6db.chwkrfaqj9m1.ap-south-1.rds.amazonaws.com:1521/ideal6db"
    }); 

    var rollno = req.body.rollno;
    var name = req.body.name;
    var age = req.body.age;
    if(req.body.rollno == undefined || req.body.name == undefined || req.body.age == undefined){
      res.status(400);
      res.send("Please enter the Data!");
    }
    else{
      var result = await connection.execute(`INSERT INTO student VALUES (:rollno, :name,:age )`,[rollno,name,age]);
    if(result){
      res.send("Data has been successfully inserted!")
    }
    }  
    
  } catch (err) {
    return res.send(err.message);
  } finally {
    if (connection) {
      try {
        await connection.close();
      } catch (err) {
        console.error(err.message);
      }
    }

  }
})


app.listen(port, () => {
  console.log("App listening on port : ", port)},
)

module.exports= app;